#########################
# SCRIPT TO POSTPROCESS MARKDOWN AFTER PROCESSING LATEX TO MD
#
# Latex ---(pandoc)---> Markdown ---(this script)---> Markdown


import re
import sys



with open(sys.argv[1], 'r') as content_file:
    content = content_file.read()

content = re.sub(r'---\nauth[\s\S]*\n---\n',
                r'',
                content)

content = re.sub(r'[$][$]([^$]*)[$][$]',
                r'\n\n\n\n\\begin{equation}\1\\end{equation}\n\n\n\n',
                content)


content = re.sub(r'\n([^\n])',
                r' \1',
                content)

content = re.sub(r'\n([^\n])',
                r'\n\n\n\1',
                content)

content = re.sub(r'\n ',
                r'\n',
                content)

content = re.sub(r'[^\n]\=\=\=(.*)\=',
                r'\n===\1=',
                content)

content = re.sub(r'[^\n]\-\-\-(.*)\-',
                r'\n---\1-',
                content)

content = re.sub(r'^ ',
                r'',
                content)

content = re.sub(r'!\[([^[]*)\[\]\{[^}]*\}\]\(([^)]*)\)\{[^}]*\}',
                r'\n\1\n\n\n<img src="bayesian-learning/\2" width="200"/>',
                content)

content = re.sub(r'\\begin\{equation\}',
                r'$$',
                content)

content = re.sub(r'\\end\{equation\}',
                r'$$',
                content)
                
content = re.sub(r'\]\(https\:\/\/www\.deep\-teaching\.org\/notebooks',
                r'](../notebooks',
                content)     
                
content = re.sub(r'\{\#sec\:(.*)\}',
                r'',
                content)            

with open(sys.argv[1], 'w') as content_file:
    content_file.write(content)

#print(content[0:])
