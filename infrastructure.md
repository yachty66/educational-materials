# Infrastructure

One of the main flaws of many research articles in the domain of machine and deep learning is that reproducing experiments can become very tedious as sometimes no code and only vague formulas are given.

And even if links to a repository containing code are shared it may be time consuming to install all software dependencies. Also if the versions of the dependencies are not frozen you might be unable to run the experiments years later when APIs have changed and you have no way to find out the correct versions of the dependencies.

For these uses cases as well as for load balancing on a cluster (only a local installation is possible as well) we developed [Curious Containers](https://www.curious-containers.cc/). For more information visit the [FAQ](https://www.curious-containers.cc/faq), the [Beginner's Guide](https://www.curious-containers.cc/docs/red-beginners-guide) or the [Installation instructions for clusters](https://www.curious-containers.cc/docs/cc-agency-installation)
