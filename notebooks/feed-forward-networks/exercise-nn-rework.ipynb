{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Neural Networks - Exercise: Simple MNIST Network"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Modules](#Python-Modules) \n",
    "* [Data](#Data)\n",
    "* [Simple MNIST Network](#Simple-MNIST-Network)\n",
    "  * [Exercise - Understanding an Implementation](#Exercise---Understanding-an-Implementation)\n",
    "  * [Exercise - Step towards a NN-Framework](#Exercise---Step-towards-a-NN-Framework)\n",
    "* [Licenses](#Licenses) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In this exercise, you will analyze an existing implementation of a neural network to recognise handwritten digits. It is an adaptation of the network presented by Michael Nielsen in his book [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/). The aim is to identify and understand how the different components that make up the network interact. With this overview of the network, you will refactor the script-like approach into a more modular implementation that clearly distinguishes between forward pass, computation of loss, backward pass and training."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "\n",
    "* Read at least [Chapter 1](http://neuralnetworksanddeeplearning.com/chap1.html) of Michael Nielsen's [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/). [(NIE15)](#NIE15)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python-Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# third party\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# internal\n",
    "from deep_teaching_commons.data.fundamentals.mnist import Mnist"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create mnist loader from deep_teaching_commons\n",
    "mnist_loader = Mnist(data_dir='data')\n",
    "\n",
    "# load all data, labels are one-hot-encoded, images are flatten and pixel squashed between [0,1]\n",
    "train_images, train_labels, test_images, test_labels = mnist_loader.get_all_data(one_hot_enc=True, normalized=True)\n",
    "\n",
    "# shuffle training data\n",
    "shuffle_index = np.random.permutation(60000)\n",
    "train_images, train_labels = train_images[shuffle_index], train_labels[shuffle_index]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simple MNIST Network\n",
    "The presented network is an adaptation of Michael Nielson's introductory example to neural networks. It is recommended, though not necessary, to read the first two chapters of his great online book ['Neural Networks and Deep Learning'](http://neuralnetworksanddeeplearning.com/) for a better understanding of the given example. Compared to the [original](https://github.com/mnielsen/neural-networks-and-deep-learning/blob/master/src/network.py) by Nielsen, the present variant was vectorized and the sigmoid activation function replaced by a rectified linear unit function (ReLU). As a result, the code is written much more compact, and the optimization of the model is much more efficient. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "delta_hist =[]\n",
    "\n",
    "def feed_forward(X, weights):\n",
    "    a = [X]\n",
    "    for w in weights:\n",
    "        a.append(np.maximum(a[-1].dot(w),0))\n",
    "    return a\n",
    "\n",
    "def grads(X, Y, weights):\n",
    "    grads = np.empty_like(weights)\n",
    "    a = feed_forward(X, weights)\n",
    "    # https://brilliant.org/wiki/backpropagation/ or https://stats.stackexchange.com/questions/154879/a-list-of-cost-functions-used-in-neural-networks-alongside-applications\n",
    "    delta = a[-1] - Y\n",
    "    delta_hist.append(np.sum(delta*Y)/len(X))\n",
    "    grads[-1] = a[-2].T.dot(delta)\n",
    "    for i in range(len(a)-2, 0, -1):\n",
    "        delta = (a[i] > 0) * delta.dot(weights[i].T)\n",
    "        grads[i-1] = a[i-1].T.dot(delta)\n",
    "    return grads / len(X)\n",
    "\n",
    "trX, trY, teX, teY = train_images, train_labels, test_images, test_labels\n",
    "weights = [np.random.randn(*w) * 0.1 for w in [(784, 200), (200,100), (100, 10)]]\n",
    "num_epochs, batch_size, learn_rate = 20, 50, 0.1\n",
    "for i in range(num_epochs):\n",
    "    for j in range(0, len(trX), batch_size):\n",
    "        X, Y = trX[j:j+batch_size], trY[j:j+batch_size]\n",
    "        weights -= learn_rate * grads(X, Y, weights)\n",
    "        once = False\n",
    "    prediction_test = np.argmax(feed_forward(teX, weights)[-1], axis=1)\n",
    "    print (i, np.mean(prediction_test == np.argmax(teY, axis=1)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise - Understanding an Implementation\n",
    "Your goal is to understand how the implementation works. Therefore you can do the following:\n",
    "\n",
    "**Task:**\n",
    "  - Plot `delta_hist`, which stores the delta value calculated on the output layer during each iteration \n",
    "  - Add comments to functions and lines of code. Follow the [Google-Python](https://google.github.io/styleguide/pyguide.html) guidelines or similar for comments.\n",
    "  - Add an argument `verbose` (boolean) to the function. When set to true, add meaningful `print` lines to the network."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Answer the Questions\n",
    "  \n",
    "After working through the implementation, try and answer the following questions:\n",
    "\n",
    "  1. Which cost function is used, what is its derivative and how is it implemented?\n",
    "  2. Why are the boundaries of your plot between [-1,0], why is it so noisy, how can you reduce the noise and what is the difference to a usual plot of a loss function?\n",
    "  3. How does the network implement the backpropagation algorithm?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise - Step towards a NN-Framework\n",
    "The presented implementation is compact and efficient, but hard to modify or extend. However, a modular design is crucial if you want to experiment with a neural network to understand the influence of its components. Now you make the first changes towards your own 'toy-neural-network-framework', which you should expand in the progress of the course. \n",
    "\n",
    "Rework the implementation from above given the classes and methods below. Again, you _do not_ have to re-engineer the whole neural network in this step. Rework the code to match the given specification and do necessary modifications only. For your understanding, you can change the names of the variables to more fitting ones."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class FullyConnectedNetwork:\n",
    "    def __init__(self, layers):\n",
    "        raise NotImplementedError(\"This is your duty\")\n",
    "        \n",
    "    def forward(self, data):\n",
    "        raise NotImplementedError(\"This is your duty\")\n",
    "\n",
    "    def backward(self, X, Y):\n",
    "        raise NotImplementedError(\"This is your duty\")\n",
    "\n",
    "    def predict(self, data):\n",
    "        raise NotImplementedError(\"This is your duty\")\n",
    "            \n",
    "class Optimizer:\n",
    "    def __init__(self, network, train_data, train_labels, test_data=None, test_labels=None, epochs=100, batch_size=20, learning_rate=0.01):\n",
    "        raise NotImplementedError(\"This is your duty\")\n",
    "        \n",
    "    def sgd(self):\n",
    "        raise NotImplementedError(\"This is your duty\")\n",
    "\n",
    "    \n",
    "# Following code should run:    \n",
    "mnist_NN = FullyConnectedNetwork([(784, 200),(200,100),(100, 10)]) \n",
    "epochs, batch_size, learning_rate = 20, 500, 0.1\n",
    "Optimizer(mnist_NN, train_images, train_labels, test_images, test_labels, epochs, batch_size, learning_rate)\n",
    "plt.plot(mnist_NN.delta_hist)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Licenses\n",
    "\n",
    "\n",
    "### Literature\n",
    "<table>\n",
    "    <tr>\n",
    "        <td><a name='NIE15'></a>[NIE15]</td>\n",
    "        <td>Michael A. Nielsen, \"Neural Networks and Deep Learning\", Determination Press, 2015 [Online]. Available: <a href='http://neuralnetworksanddeeplearning.com'>http://neuralnetworksanddeeplearning.com/</a>. [Accessed: 26-Jun-2019]</td>\n",
    "    </tr> \n",
    "</table>\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "Neural Networks - Exercise: Simple MNIST Network <br/>\n",
    "by Benjamin Voigt <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Benjamin Voigt\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
