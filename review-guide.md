# Review Guide

All notebooks contained in the this repository, are listed on the [notebooks review list](./review-list.md)
Minor reviews can be carried out by research assistants, while major reviews should be carried out by experienced machine learning experts or professors.

**Step 1**: New notebooks are `not ready` for review, which is marked accordingly in a table.

**Step 2**: As soon as a notebook is ready for the review, the notebook author creates a new issue, tagged as `Review`. The title of this issue should be `Minor Review: {Notebook Title}`. The issue is not yet assigned to a reviewer. A link to the issue must be added on the [notebooks review list](./review-list.md).

**Step 3**: Reviewers can go through the list of open reviews and pick an issue. The reviewer should assign herself to the issue and add her name to the table on the [notebooks review list](./review-list.md).
**Option 1**

* **Step 4**: The reviewer writes her review into the comments section of the open issue and assign the notebook's auther when ready.
* **Step 5**: The notebook author adresses the reviewer's concerns and changes the notebook accordingly. The issue is closed by the author when ready.
* **Step 6**: The notebook author opens a new issue for a major review titled `Major Review: {Notebook Title}` and tagged as `Review`. The major review is carried out in the same way as minor reviews (see steps 1 to 5).

**Option 2**

* **Step 4**: The reviewer makes the according changes in the notebook AND keeps track of the changes inside the git issue. This means, writing out what a notebook cell was exactly before the changes, and how it looks now after the change.
* **Step 5**: The notebook author reads through the changes and correts them if needed and closes the issue.
* **Step 6**: The notebook author opens a new issue for a major review titled `Major Review: {Notebook Title}` and tagged as `Review`. The major review is carried out in the same way as minor reviews (see steps 1 to 5).

This process ensures that reviewers can easily indetify notebooks, which are for review. Users can easily identify, which notebooks are reviewed, which means that the information in the notebook is more likely to be **complete**, **correct** and **concise**. 
